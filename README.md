# assessment


Section A : Making a simple website with JS

Section A : Making a simple website with JS
Task 1 - BootStrap
a. Use this page, and create a modal - which gets called - whenever the user clicks on the pricing. The modal should contain a form - asking following details:

  Name

  Email

  Order Comments
  
From the bootstrap template, one should be able to click on any of the buttons of the three pricing tables, and fill in a form.


b. Use a slider - which should allow user to scroll between the number of users - and suppose if the number of users are 0-10, then first plan should get highlighted, 10-20, the second plan should get highlighted, and so on.
Task 2 - JS

Sign up on https://forms.maakeetoo.com

from task 1 - the contents of form submission - should be populated there.

screen shot


![Alt text](<Screenshot (4).png>)

![Alt text](<Screenshot (5).png>)

video output

<video src="Bootstrap%20Modal%20and%20Slider%20-%20Personal%20-%20Microsoft%E2%80%8B%20Edge%202023-12-07%2023-51-03.mp4" controls title="Title"></video>


deploy link

https://65721b62d7dec20090f185ca--fantastic-donut-aaf613.netlify.app/


